
public class BinärsucheVergleich {
	public static String binäreSuche(int zahlen[],int x) {
		int helfte = zahlen.length/2;
		for(int i = 0;i<zahlen.length;i++) {
			if(x == zahlen[helfte]) {
				return "Zahl gefunden";
				}
			if(x < zahlen[helfte]) {
				helfte = helfte / 2;
			}
			if(x > zahlen[helfte]) {
				helfte = helfte + (helfte / 2);
			}
		}
		return "Zahl nicht gefunden";
	}
	public static int getVergleiche2(int zahlen[],int x){
		int vergleiche = 0;
			for(int i = 0; i <zahlen.length;i++) {
				vergleiche++;
				if(x == zahlen[i]) {
					return vergleiche;
					}
				}
			return vergleiche;
		}
}
