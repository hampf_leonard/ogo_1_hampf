import java.util.Arrays;
public class BubbleSortTest {
		public static void main (String[]args){
			BubbleSort bs = new BubbleSort();
			
			long[] zahlenliste = new long[2000];
			int f = 0;
			for(int i = 2000;i>0;i--) {
				zahlenliste[f]=i;
				f++;
			}
			
			// Vor Sortierung
			System.out.println(Arrays.toString(zahlenliste));
			
			// Sortierung
			bs.sortiere(zahlenliste);
			
			// Nach Sortierung
			System.out.println(Arrays.toString(zahlenliste));
			System.out.println(bs.getVertauschungen());
		
		}
	}
