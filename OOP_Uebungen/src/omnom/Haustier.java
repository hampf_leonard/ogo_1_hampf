package omnom;

public class Haustier {
	//Atribute
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	private String name;
	//Kronstruktor
	public Haustier() {
		this.hunger = 0;
		this.muede = 0;
		this.zufrieden = 0;
		this.gesund = 0;
		this.name = "";
	}
	public Haustier(String name) {
		this.name = name;
	}
	public void setHunger(int hunger) {
		if(hunger < 0) {
			this.hunger = 0;
		}
		if(hunger > 100) {
			this.hunger = 100;
		}
		this.hunger = hunger;
	}
	public int getHunger() {
		return hunger;
	}
	public void setMuede(int muede) {
		if(muede < 0) {
			this.muede = 0;
		}
		if(muede > 100) {
			this.muede = 100;
		}
		this.muede = muede;
	}
	public int getMuede() {
		return muede;
	}
	public void setZufrieden(int zufrieden) {
		if(zufrieden < 0) {
			this.zufrieden = 0;
		}
		if(zufrieden > 100) {
			this.zufrieden = 100;
		}
		this.zufrieden = zufrieden;
	}
	public int getZufrieden() {
		return zufrieden;
	}
	public void setGesund(int gesund) {
		if(gesund < 0) {
			this.gesund = 0;
		}
		if(gesund > 100) {
			this.gesund = 100;
		}
		this.gesund = gesund;
	}
	public int getGesund() {
		return gesund;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public int fuettern(int anzahl) {
		this.hunger = hunger + anzahl;
		return anzahl;
	}
	public int schlafen(int dauer) {
		this.muede = muede + dauer;
		return dauer;
	}
	public int spielen(int dauer1) {
		this.zufrieden = zufrieden + dauer1;
		return dauer1;
	}
	public void heilen() {
		this.gesund = 100;
	}
}
