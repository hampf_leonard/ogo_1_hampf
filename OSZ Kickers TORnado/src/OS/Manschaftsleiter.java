package OS;
//Ok
public class Manschaftsleiter extends Mensch{
//Atribute
	public String nameMannschaft;
	public String persönlichesEngagement;
//KOnstruktor
	public Manschaftsleiter() {
	}
	public Manschaftsleiter(String persönlichesEngagement, String nameMannschaft, int telefonNr, String name,boolean jahresBetragBezahlt) {
		super(telefonNr, name, jahresBetragBezahlt);
		this.persönlichesEngagement = persönlichesEngagement;
		this.nameMannschaft = nameMannschaft;
	}
//VerwaltungsMethoden
	public void setPersöhnlichesEngagement(String persönlichesEngagement) {
		this.persönlichesEngagement = persönlichesEngagement;
	}
	public String getPersönlichesEngagement() {
		return persönlichesEngagement;
	}
}
