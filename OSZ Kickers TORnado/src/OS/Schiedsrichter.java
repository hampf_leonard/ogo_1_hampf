package OS;
//ok
public class Schiedsrichter extends Mensch{
//Atribute
	public int anzahlGepfiffeneSpiele;
//Konstruktor
	public Schiedsrichter(){
	}
	public Schiedsrichter(int anzahlGepfiffeneSpiele, int telefonNr, String name,boolean jahresBetragBezahlt) {
		super(telefonNr, name, jahresBetragBezahlt);
		this.anzahlGepfiffeneSpiele = anzahlGepfiffeneSpiele;
	}
//Verwaltungsmethoden
	public void setAnzahlGepfiffeneSpiele(int anzahlGepfiffeneSpiele) {
		this.anzahlGepfiffeneSpiele = anzahlGepfiffeneSpiele;
	}
	public int getAnzahlGepfiffeneSpiele() {
		return anzahlGepfiffeneSpiele;
	}
}
