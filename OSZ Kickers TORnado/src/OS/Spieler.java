package OS;
//ok
public class Spieler extends Mensch {
//Atribute
	private int trikoNr;
	private String position;
//Konstruktor
	public Spieler() {
		}
	public Spieler(int trikoNr, String position, int telefonNr, String name, boolean jahresBetragBezahlt) {
		super(telefonNr, name, jahresBetragBezahlt);
		this.trikoNr = 0;
		this.position = "";
	}
//Verwaltungsmethoden
	public void setTrikoNr(int trikoNr) {
		this.trikoNr = trikoNr;
	}
	public int getTrikoNr() {
		return trikoNr;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getPosition() {
		return position;
	}
}
