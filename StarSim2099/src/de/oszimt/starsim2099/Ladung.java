package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Ladung {

	// Attribute
	private String typ;
	private int masse;
	private double posX;
	private double posY;
	// Methoden
	public Ladung() {
		this.typ = "";
		this.masse = 0;
		this.posX = 0;
		this.posY = 0;
	}
	// Methoden
	public void setTyp(String Typ) {
		this.typ = typ;
	}
	public String getTyp() {
		return typ;
	}
	public void setMasse(int masse) {
		this.masse = masse;
	}
	public int getMasse() {
		return masse;
	}
	public void setPosX(double posX) {
		this.posX = posX;
	}
	public double getPosX() {
		return posX;
	}
	public void setPosY(double posY) {
		this.posY = posY;
	}
	public double getPosY() {
		return posY;
	}
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}