package de.oszimt.starsim2099;

/**
 * Write a description of class Planet here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Planet {

	// Attribute
	private String name;
	private int anzahlHafen;
	private double posX;
	private double posY;
	// Methoden
	public Planet() {
		this.name = "";
		this.anzahlHafen = 0;
		this.posX = 0;
		this.posY = 0;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setAnzahlHafen(int anzahlHafen) {
		this.anzahlHafen = anzahlHafen;
	}
	public int getAnzahlHafen() {
		return anzahlHafen;
	}
	public void setPosX(double posX) {
		this.posX = posX;
	}
	public double getPosX() {
		return posX;
	}
	public void setPosY(double posY) {
		this.posY = posY;
	}
	public double getPosY() {
		return posY;
	}
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] planetShape = { { '\0', '/', '*', '*', '\\', '\0' }, { '|', '*', '*', '*', '*', '|' },
				{ '\0', '\\', '*', '*', '/', '\0' } };
		return planetShape;

	}
}
