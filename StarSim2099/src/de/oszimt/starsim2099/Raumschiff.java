package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff {

	// Attribute
	private String typ;
	private String antrieb;
	private int maxLadekapazitaet;
	private int winkel;
	private double posX;
	private double posY;
	// Methoden
	public Raumschiff() {
		this.typ = "";
		this.antrieb = "";
		this.maxLadekapazitaet = 0;
		this.winkel = 0;
		this.posX = 0;
		this.posY = 0;
	}
	public void setTyp(String Typ) {
		this.typ = typ;
	}
	public String getTyp() {
		return typ;
	}
	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}
	public String getAntrieb() {
		return antrieb;
	}
	public void setMaxLadekapazitaet(int maxLadekapazitaet) {
		this.maxLadekapazitaet = maxLadekapazitaet;
	}
	public int getMaxLadekapazitaet() {
		return maxLadekapazitaet;
	}
	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}
	public int getWinkel() {
		return winkel;
	}
	public void setPosX(double posX) {
		this.posX = posX;
	}
	public double getPosX() {
		return posX;
	}
	public void setPosY(double posY) {
		this.posY = posY;
	}
	public double getPosY() {
		return posY;
	}
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},				
		};
		return raumschiffShape;
	}

}
