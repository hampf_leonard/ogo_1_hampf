package git_taschenrechner;

public class Taschenrechner {

	public double add(double zahl1, double zahl2) {
		return zahl1 + zahl2;
	}

	public double sub(double zahl1, double zahl2) {
		return zahl1-zahl2;
	}

	public double mul(double zahl1, double zahl2) {
		return zahl1 * zahl2;
	}

	public double div(double zahl1, double zahl2) {
		return zahl1 / zahl2;
	}
	//
	public String pq(double zahl1,double zahl2,String s) {
		double part1 = zahl1 / 2;
		double part2 = (zahl1*zahl1)-zahl2;
		//System.out.println(part2);
		double erg1 = -part1 + Math.sqrt(part2);
		double erg2 = -part1 - Math.sqrt(part2);
		if(part2 >= 0) {
			s ="x1 = "+erg1+" x2 = "+erg2;
		}
		else {
	   s = "keine nullstelle";
		}
	   return s;
	   //Quadieren
	}
	public double quad(double zahl1, double zahl2) {
		double erg = zahl1;
		for(int i = 0; i<zahl2-1;i++) {
			erg = erg * zahl1;
			}
		return erg;
	}
	//akkuAdd
	public double add1(double akku, double akku1) {
		akku = akku + akku1;
		return akku;
	}
	//akkuSub
	public double sub1(double akku, double akku1) {
		akku = akku - akku1;
		return akku;
	}
	//akkuMul
	public double mul1(double akku, double akku1) {
		akku = akku * akku1;
		return akku;
	}
	//akkuDiv
	public double div1(double akku, double akku1) {
		akku = akku / akku1;
		return akku;
	}
	public void wurzel() {
	}
}
}
