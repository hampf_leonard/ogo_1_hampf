
public class Einschaetzungstest extends Test {
//Atribute
	protected Ballon ballon;
	protected Quadrat quadrat;
//Konstruktor
	public Einschaetzungstest() {
		super();
		this.ballon = ballon;
		this.quadrat = quadrat;
	}
	public Einschaetzungstest(Aktiv aktiv, Ergebnis ergebnis, Ballon ballon, Quadrat quadrat){
		super(aktiv, ergebnis);
		this.quadrat = quadrat;
		this.ballon = ballon;
	}
//setter und getter
	public Ballon getBallon() {
		return ballon;
	}
	public void setBallon(Ballon ballon) {
		this.ballon = ballon;
	}
	public Quadrat getQuadrat() {
		return quadrat;
	}
	public void setQuadrat(Quadrat quadrat) {
		this.quadrat = quadrat;
	}
//Methoden
	public void starten() {
		
	}
	public void stoppen() {
		
	}
	public void zeighilfe() {
		
	}
}
