
public class Konzentrationstest extends Test {
//Attribute
	protected Stoppuhr stoppuhr;
//Konstruktor
	public Konzentrationstest(){
		super();
		this.stoppuhr = stoppuhr;
	}
	public Konzentrationstest(Aktiv aktiv, Ergebnis ergebnis, Stoppuhr stoppuhr) {
		super(aktiv, ergebnis);
		this.stoppuhr = stoppuhr;
	}
//setter und getter
	public Stoppuhr getStoppuhr() {
		return stoppuhr;
	}
	public void setStoppuhr(Stoppuhr stoppuhr) {
		this.stoppuhr = stoppuhr;
	}
//Methoden
	public void starten() {
			
	}
	public void stoppen() {
			
	}
	public void zeighilfe() {
			
	}
}
