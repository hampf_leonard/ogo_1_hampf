
public class Reaktionstest extends Test {
//Atribute
	protected Ampel ampel;
	protected Stoppuhr stoppuhr;
//Konstruktor
	public Reaktionstest() {
		super();
		this.ampel = ampel;
		this.stoppuhr = stoppuhr;
	}
	public Reaktionstest(Aktiv aktiv, Ergebnis ergebnis, Ampel ampel, Stoppuhr stoppuhr) {
		super(aktiv, ergebnis);
		this.ampel = ampel;
		this.stoppuhr = stoppuhr;
	}
//Setter und Getter
	public Ampel getAmpel() {
		return ampel;
	}
	public void setAmpel(Ampel ampel) {
		this.ampel = ampel;
	}
	public Stoppuhr getStoppuhr() {
		return stoppuhr;
	}
	public void setStoppuhr(Stoppuhr stoppuhr) {
		this.stoppuhr = stoppuhr;
	}
//Methoden
	public void starten() {
		
	}
	public void stoppen() {
		
	}
	public void zeigHilfe() {
		
	}
}
