
public class Test {
//Attribute
	protected Aktiv aktiv;
	protected Ergebnis ergebnis;
//Konstruktor
	public Test() {
		this.aktiv = aktiv;
		this.ergebnis = ergebnis;
	}
	public Test(Aktiv aktiv, Ergebnis ergebnis) {
		this.aktiv = aktiv;
		this.ergebnis = ergebnis;
	}
//Setter und Getter
	public Aktiv getAktiv() {
		return aktiv;
	}
	public void setAktiv(Aktiv aktiv) {
		this.aktiv = aktiv;
	}
	public Ergebnis getErgebnis() {
		return ergebnis;
	}
	public void setErgebnis(Ergebnis ergebnis) {
		this.ergebnis = ergebnis;
	}
//Methoden
	protected void warten() {
		
	}
	public void isAktiv() {
		
	}
	public void starten() {
		
	}
	public void stoppen() {
		
	}
	public void zeigHilfe() {
		
	}
}
