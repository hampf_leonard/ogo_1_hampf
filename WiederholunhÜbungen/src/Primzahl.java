public class Primzahl{
	public boolean isPrimzahl(long zahl) {
		long v = 0;
		boolean isprimzahl = true;
		for(int i = 2; i < zahl;i++) {
			v = zahl % i;
			if(v == 0) {
				isprimzahl = false;	
			}
		}
		return isprimzahl;
	}
}