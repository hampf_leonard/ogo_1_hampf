package srcc;

public class Primzahl1 {
	
	public static boolean isPrimzahl1(long zahl) {
		long v = 0;
		final long timeStart = System.currentTimeMillis();
		boolean isPrimzahl = true;
			for(int i = 2;i< zahl;i++){
				v = zahl % i;
				if(v == 0) {
					isPrimzahl = false;	
					}
			}
		final long timeEnd = System.currentTimeMillis();
        System.out.println("Verlaufszeit der Schleife: " + (timeEnd - timeStart) + " Millisek.");
		return isPrimzahl;
	}
}

