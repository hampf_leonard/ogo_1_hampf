package zootycoon;

public class Addon {
	//Atribute
	private int idNr;
	private String bezeichnung;
	private double preis;
	private int maxBestand;
	
	//konstruktor
	public Addon() {
		idNr = 0;
		bezeichnung = "";
		preis = 0;
		maxBestand = 0;
	}
	//Verwaltungsmethoden
	public int getIdNr() {
		return idNr;
	}
	public void setIdNr(int idNr) {
		this.idNr = idNr;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public double getPreis() {
		return preis;
	}
	public  void setPreis(double preis) {
		this.preis = preis;
	}
	public int getMaxBestand() {
		return maxBestand;
	}
	public void setMaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}
	//methoden
	public int addonKaufen(int max) {
		max = max - 1;
		if(max < 0) {
			max = 0;
			System.out.println("Der Maximalebestand ist aufgebraucht.");
		}
	return max;
	}
	public int addonVerbrauchen(int anzahl) {
		anzahl = anzahl - 1;
		if(anzahl < 0) {
			anzahl = 0;
			System.out.println("Sie haben keine addons zum verbrauchen");
		}
	return anzahl;
	}	
	public double zeigeGesamtwertAddons(int anzahl, double geld) {
		geld = anzahl * geld;
	return geld;
	}
}
