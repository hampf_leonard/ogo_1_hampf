package zootycoon;
import java.util.Scanner;
public class AddonTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int swValue;
		int chose;
		int anzahl = 0;
		Addon addon02 = new Addon();
		addon02.setIdNr(6734);
		addon02.setBezeichnung("Premieum Attraktion");
		addon02.setPreis(2.99);
		addon02.setMaxBestand(5);
		int max = addon02.getMaxBestand();
		double geld = addon02.getPreis();
		do {
		System.out.println("Was m�chten sie tun?");
		System.out.println("---------------------");
		System.out.println("(1) Ein Addon kaufen ");
		System.out.println("(2) Addon benutzen");
		System.out.println("(3) Gesamtwert der Addons anzeige lassen");
		System.out.println("---------------------");
		swValue = sc.next().charAt(0);
		switch (swValue) {
 		case '1':
 			System.out.println("Sie k�nnen das Produkt noch "+addon02.addonKaufen(max)+" mal kaufen.");
			if(max > 1) {
				System.out.println("Sie haben erfolgreich das Addon "+addon02.getBezeichnung()+" f�r "+addon02.getPreis());
			}
			max = max -1;
			anzahl = anzahl + 1;
			break;
 		case '2':
			System.out.println("Addon wurde verbraucht. Sie haben noch "+addon02.addonVerbrauchen(anzahl)+ " addons");
 			anzahl = addon02.addonVerbrauchen(anzahl);
			break;
 		case '3':
		System.out.println("Das ist der Gesamtwert der bisher gekauften Addons "+addon02.zeigeGesamtwertAddons(anzahl,geld));
 			break;
 		default:
 			System.out.println("Nicht m�glich");
 			break;
		}
		System.out.println("M�chten sie fortfahren(j).");
 		chose = sc.next().charAt(0);
		}
 		while(chose == 'j');
	}
}
